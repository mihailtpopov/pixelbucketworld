﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PixeBucketWorld.Data.CustomDataAnnotations
{
    public class GreaterThanAttribute : ValidationAttribute
    {
        private readonly string incomingPropertyName;

        public GreaterThanAttribute(string incomingPropertyName, string errorMessage) 
            : base(errorMessage)
        {
            this.incomingPropertyName = incomingPropertyName;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var greaterDate = (DateTime)value;
            var incomingDate = (DateTime)validationContext.ObjectInstance
                .GetType()
                .GetProperty(incomingPropertyName)
                .GetValue(validationContext.ObjectInstance);

            if (greaterDate > incomingDate)
            {
                return ValidationResult.Success;
            }

            return new ValidationResult(base.ErrorMessage);
        }
    }
}
