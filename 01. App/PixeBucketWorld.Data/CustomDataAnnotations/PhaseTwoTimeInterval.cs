﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PixeBucketWorld.Data.CustomDataAnnotations
{
    public class PhaseTwoTimeInterval : ValidationAttribute
    {
        private readonly int hours;
        public PhaseTwoTimeInterval(int hours)
        {
            this.hours = hours;
            base.ErrorMessage = $"Phase two time interval must be no more than {hours} hours";
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var phaseTwoFinishingDate = (DateTime)value;

            var phaseTwoStartingDate = (DateTime)validationContext
                .ObjectInstance
                .GetType()
                .GetProperty("PhaseTwoStartingDate")
                .GetValue(validationContext.ObjectInstance);

            if ((phaseTwoFinishingDate - phaseTwoStartingDate).TotalHours <= hours)
            {
                return ValidationResult.Success;
            }

            return new ValidationResult(base.ErrorMessage);
        }
    }
}
