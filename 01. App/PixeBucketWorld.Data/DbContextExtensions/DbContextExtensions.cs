﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace PixeBucketWorld.Data.DbContextExtensions
{
    public static class DbContextExtensions
    {
        public static void DetachAllEntities(this PBWDbContext context)
        {
            var changedEntriesCopy = context.ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Added ||
                            e.State == EntityState.Modified ||
                            e.State == EntityState.Deleted)
                .ToList();

            foreach (var entry in changedEntriesCopy)
                entry.State = EntityState.Detached;
        }
    }
}
