﻿using PixeBucketWorld.Data.Contracts;
using PixeBucketWorld.Data.CustomDataAnnotations;
using PixeBucketWorld.Data.ValidationAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PixeBucketWorld.Data.Models
{
    public class Contest : IUserFilteringCategory
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Title")]
        [Required, StringLength(50, MinimumLength = 5, ErrorMessage = "{0} length must be between {2} and {1} characters")]
        public string Name { get; set; }

        [ForeignKey("CategoryId")]
        public int CategoryId { get; set; }
        public Category Category { get; set; }

        [ForeignKey("TypeId")]
        public int TypeId { get; set; }
        public ContestType Type { get; set; }

        [CurrentDate("Date must be in future")]
        public DateTime PhaseOneStartingDate {get; set;}

        [GreaterThan(nameof(PhaseOneStartingDate), "Phase I finishing date must be greater than the starting date")]
        public DateTime PhaseOneFinishingDate { get; set; }

        [GreaterThan(nameof(PhaseOneFinishingDate), "Phase II starting date must be greater than Phase I finishing date")]
        public DateTime PhaseTwoStartingDate { get; set; }

        [GreaterThan(nameof(PhaseTwoStartingDate), "Phase II finishing date must be greater than the starting date")]
        public DateTime PhaseTwoFinishingDate { get; set; }

        [ForeignKey("PhaseId")]
        public int PhaseId { get; set; }

        public Phase CurrentPhase { get; set; }

        public string PhotoUrl { get; set; }

        public List<ContestParticipantLink> ParticipantsLink { get; set; } = new List<ContestParticipantLink>();

        public List<ContestJurorLink> JurorsLink { get; set; } = new List<ContestJurorLink>();

        public List<Photo> UploadedPhotos { get; set; } = new List<Photo>();
    }
}
