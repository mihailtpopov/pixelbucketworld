﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PixeBucketWorld.Data.Models.Logging.Enums
{
    public enum InformationLogEvent
    {
        INSERT_ITEM = 1003,
        UPDATE_ITEM = 1004,
        DELETE_ITEM = 1005,
    };
}
