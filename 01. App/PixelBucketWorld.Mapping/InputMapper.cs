﻿using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Map.InputModels.InputContestModels;
using PixelBucketWorld.Map.InputModels.InputUserModels;
using System;

namespace PixelBucketWorld.Map
{
    public static class InputMapper
    {
        public static Contest Map(CreateContestViewModel inputContest)
        {
            return new Contest()
            {
                TypeId = inputContest.Type.Value,
                CategoryId = inputContest.Category.Value,
                Name = inputContest.Title,
                PhaseOneStartingDate = inputContest.PhaseOneStartingDate,
                PhaseOneFinishingDate = inputContest.PhaseOneFinishingDate,
                PhaseTwoStartingDate = inputContest.PhaseTwoStartingDate,
                PhaseTwoFinishingDate = inputContest.PhaseTwoFinishingDate,
                PhaseId = 1,
            };
        }

        public static User Map(InputRegisterUser inputUser)
        {
            return new User()
            {
                FirstName = inputUser.FirstName,
                LastName = inputUser.LastName,
                UserName = inputUser.Username,
                Email = inputUser.Email,
                RankId = 1
                
            };
        }
    }
}
