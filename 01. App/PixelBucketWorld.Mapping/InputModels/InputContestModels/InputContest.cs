﻿using Foolproof;
using PixeBucketWorld.Data.ValidationAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PixelBucketWorld.Map.InputModels.InputContestModels
{
    public class InputContestModel
    { 
        [StringLength(50, MinimumLength = 5, ErrorMessage = "{0} length must be between {2} and {1} characters")]
        public string Title { get; set; }

        public int? Category { get; set; }

        public int? Type { get; set; }

        [Required, CurrentDate("Date must be in future")]
        public DateTime PhaseOneStartingDate { get; set; }

        [Required]
        public DateTime PhaseOneFinishingDate { get; set; }

        [Required]
        public DateTime PhaseTwoStartingDate { get; set; }

        [Required]
        public DateTime PhaseTwoFinishingDate { get; set; }
    }
}
