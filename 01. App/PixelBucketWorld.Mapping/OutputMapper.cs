﻿using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Map.OutputModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace PixelBucketWorld.Map
{
    public static class OutputMapper
    {
        public static OutputContest Map(Contest contest)
        {
            return new OutputContest()
            {
                TypeId = contest.TypeId,
                CategoryId = contest.CategoryId,
                Title = contest.Name,
                PhaseOneStartingDate = contest.PhaseOneStartingDate,
                PhaseOneFinishingDate = contest.PhaseOneFinishingDate,
                PhaseTwoStartingDate = contest.PhaseTwoStartingDate,
                PhaseTwoFinishingDate = contest.PhaseTwoFinishingDate
            };
        }
    }
}
