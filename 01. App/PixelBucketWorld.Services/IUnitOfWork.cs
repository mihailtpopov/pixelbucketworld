﻿using PixelBucketWorld.Services.Services;
using System.Threading.Tasks;

namespace PixelBucketWorld.Services
{
    public interface IUnitOfWork
    {
        PhotoService PhotoService { get; }

        ContestService ContestService { get; }

        UserService UserService { get; }

        CategoryService CategoryService { get; }

        MessageService MessageService { get; }

        RankService RankService { get; }

        RoleService RoleService { get; }

        PhotoAssesmentService PhotoAssessmentService { get; }

        ContestParticipantLinkService ContestParticipantLinkService { get; }

        ContestJurorLinkService ContestJurorLinkService { get; }

        ContestTypesService ContestTypesService { get; }

        Task SaveAsync();
    }
}
