﻿using Microsoft.EntityFrameworkCore;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Consts.Strings;
using System.Data;
using System.Threading.Tasks;

namespace PixelBucketWorld.Services.Services
{
    public class CategoryService : GenericService<Category>
    {
        public CategoryService(PBWDbContext context)
            : base(context)
        {

        }

        public override async Task<Category> CreateAsync(Category entity)
        {
            var category = await this.dbSet.FirstOrDefaultAsync(c => c.Name == entity.Name);

            if (category != null)
            {
                throw new DuplicateNameException(ErrorConsts.ENTITY_EXISTS);
            }

            await this.dbSet.AddAsync(entity);

            return entity;
        }

    }
}
