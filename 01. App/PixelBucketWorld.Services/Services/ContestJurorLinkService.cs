﻿using Microsoft.EntityFrameworkCore;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PixelBucketWorld.Services.Services
{
    public class ContestJurorLinkService : GenericService<ContestJurorLink>
    {

        public ContestJurorLinkService(PBWDbContext context) :
            base(context)
        {

        }

        public async Task CreateDefaultContestJurorLinks(IEnumerable<int> userIds, int contestId)
        {
            if (userIds == null)
            {
                throw new ArgumentNullException();
            }

            foreach (var userId in userIds)
            {
                await CreateDefaultContestJurorLink(userId, contestId);
            }
        }

        public async Task CreateDefaultContestJurorLink(int userId, int contestId)
        {
            await this.context.ContestJurorLinks.AddAsync(new ContestJurorLink()
            {
                ContestId = contestId,
                UserId = userId,
                IsAccepted = false,
            });
        }

        public ContestJurorLink UpdateContestInvitationState(ContestJurorLink cjl)
        {
            cjl.IsAccepted = true;
            cjl.AcceptanceTime = DateTime.Now;

            return cjl;
        }

        public async Task<ContestJurorLink> GetAsync(int userId, int contestId)
        {
            var contestJurorLink = await this.dbSet.FirstOrDefaultAsync(cjl => cjl.UserId == userId && cjl.ContestId == contestId);

            return contestJurorLink;
        }

    }
}
