﻿using Microsoft.EntityFrameworkCore;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PixelBucketWorld.Services.Services
{
    public class ContestParticipantLinkService : GenericService<ContestParticipantLink>
    {
        public ContestParticipantLinkService(PBWDbContext context) :
            base(context)
        {

        }

        public async Task CreateDefaultContestParticipantLinks(IEnumerable<int> userIds, int contestId)
        {
            if (userIds == null)
            {
                throw new ArgumentNullException();
            }

            foreach (var userId in userIds)
            {
                await CreateDefaultContestParticipantLink(userId, contestId);
            }
        }

        public async Task<ContestParticipantLink> CreateDefaultContestParticipantLink(int userId, int contestId)
        {
            var newCPL = new ContestParticipantLink()
            {
                ContestId = contestId,
                UserId = userId,
                IsAccepted = false,
                ParticipationDate = null,
                UploadingPhotoTime = null
            };

            await this.context.ContestParticipantLinks.AddAsync(newCPL);
            return newCPL;
        }

        public ContestParticipantLink UpdateContestInvitationState(ContestParticipantLink cpl)
        {
            cpl.IsAccepted = true;
            cpl.ParticipationDate = DateTime.Now;

            return cpl;
        }

        public ContestParticipantLink UpdatePhotoUploadingState(ContestParticipantLink cpl)
        {
            cpl.ParticipationDate = DateTime.Now;
            cpl.UploadingPhotoTime = DateTime.Now;

            return cpl;
        }

        public async Task<ContestParticipantLink> GetAsync(int userId, int contestId)
        {
            var contestParticipantLink = await this.context.ContestParticipantLinks.FirstOrDefaultAsync(cpl=>cpl.UserId == userId && cpl.ContestId == contestId);

            return contestParticipantLink;
        }
    }
}
