﻿using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Contracts;
using PixeBucketWorld.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PixelBucketWorld.Services.Services
{
    public class RoleService : GenericService<Role>
    {
        public RoleService(PBWDbContext context)
            : base(context)
        {

        }

        public async Task<IEnumerable<IUserFilteringCategory>> GetAllAsFilteringCategoryAsync()
        {
            var roles = await base.GetAllAsync();
            return roles;
        }   
    }
}
