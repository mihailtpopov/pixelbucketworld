﻿using Microsoft.EntityFrameworkCore;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using PixeBucketWorld.Data.Models.FilteringModels;
using PixelBucketWorld.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PixelBucketWorld.Services.Services
{
    public class UserService : GenericService<User>
    {
        private readonly RoleService roleService;

        public UserService(PBWDbContext context, RoleService roleService)
            : base(context)
        {
            this.roleService = roleService;
        }

        public async Task<IEnumerable<User>> GetAsync(UserFilteringCriterias criterias)
        {
            IQueryable<User> users = this.context.Users
                .Include(u => u.Rank);

            //passing filtration if all roles are choosen
            if (criterias.RoleSelectOptions.Any() && criterias.RoleSelectOptions.Count() != this.context.Roles.Count())
            {
                var userIdsInRole = this.context.UserRoles
                    .Where(ur => criterias.RoleSelectOptions.Contains(ur.RoleId))
                    .Select(ur => ur.UserId);

                users = users.Where(u => userIdsInRole.Contains(u.Id));
            }

            if (criterias.RankSelectOptions.Any())
            {
                users = users.Where(u => u.RankId != null && criterias.RankSelectOptions.Contains(u.RankId.Value));
            }

            if (criterias.FirstName != null)
            {
                users = users.Where(u => u.FirstName == criterias.FirstName);
            }

            if (criterias.LastName != null)
            {
                users = users.Where(u => u.LastName == criterias.LastName);
            }


            return await users.OrderByDescending(u => u.CurrentRankingPoints).ToListAsync();
        }

        public async Task LoadRankAsync(User user)
        {
            await this.context.Entry(user).Reference(u => u.Rank).LoadAsync();
        }

        public async Task<int?> GetPointsTillNextRankAsync(int userId)
        {
            var user = await this.context.Users.FirstOrDefaultAsync(u => u.Id == userId);
            int? pointsTillNextRank = null;

            if (user.RankId.HasValue)
            {
                if (user.RankId < this.context.Ranks.Max(r => r.Id))
                {
                    var nextRank = await this.context.Ranks.FirstOrDefaultAsync(r => r.Id == user.RankId + 1);

                    pointsTillNextRank = nextRank.RequiredPoints - user.CurrentRankingPoints.Value;
                }
            }

            return pointsTillNextRank;
        }

        public void SetUserInitialDetails(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException();
            }

            user.RankId = 1;
            user.CurrentRankingPoints = 0;
        }

        public async Task<bool> IsUserJurorAsync(int userId, int? contestId = null)
        {
            var user = await this.dbSet.FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            if (contestId !=null)
            {
                if (this.context.ContestJurorLinks.Any(cjr => cjr.UserId == userId && cjr.ContestId == contestId))
                {
                    return true;
                }
            }
            else
            {
                if (this.context.ContestJurorLinks.Any(cjr => cjr.UserId == userId))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
