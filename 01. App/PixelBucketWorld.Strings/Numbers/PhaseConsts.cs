﻿namespace PixelBucketWorld.Consts.Numbers
{
    public class PhaseConsts
    {
        public const int PRESTART = 1;
        public const int PHASE_ONE = 2;
        public const int TRANSITION = 3;
        public const int PHASE_TWO = 4;
        public const int FINISHED = 5;
    }
}
