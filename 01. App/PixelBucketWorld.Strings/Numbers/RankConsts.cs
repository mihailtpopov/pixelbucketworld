﻿namespace PixelBucketWorld.Consts.Numbers
{
    public static class RankConsts
    {
        public const int JUNKIE = 1;
        public const int ENTHUSIAST = 2;
        public const int MASTER = 3;
        public const int WISE_AND_BENEVOLENT_PHOTO_DICTATOR = 4;
    }
}
