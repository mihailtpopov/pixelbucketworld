﻿namespace PixelBucketWorld.Consts.Numbers
{
    public static class UserRoles
    {
        public const int ORGANISER_ROLE_ID = 1;
        public const int PARTICIPANT_ROLE_ID = 2;
    }
}
