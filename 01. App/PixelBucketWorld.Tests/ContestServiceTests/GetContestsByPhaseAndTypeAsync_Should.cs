﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.ContestServiceTests
{
    [TestClass]
    public class GetContestsByPhaseAndTypeAsync_Should
    {
        [TestMethod]
        public async Task GetContestsByPhaseAndTypeAsync_Should_ReturnAll_IfNoFilterPassed()
        {
            // Arrange

            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                // Act
                var contests = await unitOfWork.ContestService.GetContestsByPhaseAndTypeAsync();
                int count = context.Contests.Count();

                // Assert
                Assert.AreEqual(count, contests.Count());

            }
        }

        [TestMethod]
        [DataRow(1, 6)]
        [DataRow(2, 2)]
        public async Task GetContestsByPhaseAndTypeAsync_Should_ReturnCorrectCount_IfFilteredByType(int typeId, int expectedCount)
        {
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                // Act
                var contests = await unitOfWork.ContestService.GetContestsByPhaseAndTypeAsync(typeId:typeId);

                // Assert
                Assert.AreEqual(expectedCount, contests.Count());
            }
        }

        [TestMethod]
        [DataRow(1, 1)] // phase prestart
        [DataRow(2, 2)] // phase I
        [DataRow(4, 1)] // phase II
        [DataRow(5, 4)] // finished
        public async Task GetContestsByPhaseAndTypeAsync_Should_ReturnCorrectCount_IfFilteredByPhase
            (int phaseeId, int expectedCount)
        {
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                // Act
                var contests = await unitOfWork
                    .ContestService.GetContestsByPhaseAndTypeAsync(phaseId: phaseeId);

                // Assert
                Assert.AreEqual(expectedCount, contests.Count());
            }
        }


    }
}
