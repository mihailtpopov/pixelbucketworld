﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.LoggingServiceTests
{
    [TestClass]
    public class LogErrorAsync_Should
    {
        [TestMethod]
        public async Task LogErrorAsync_ShouldAddLogEntryToLogs()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                //Arrange
                Util.SeedDatabase(context);

                var loggingService = new LoggingService(context);
                var user = new User();

                //Act
                var currLogsCount = context.Logs.Count();
                await loggingService.LogErrorAsync(new ArgumentException());

                //Assert
                Assert.AreEqual(currLogsCount + 1, context.Logs.Local.Count());
            }
        }

        [TestMethod]
        public async Task LogErrorAsync_ShouldThrowArgumentNullException_IfNullParameterIsPassed()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                //Arrange
                Util.SeedDatabase(context);

                var loggingService = new LoggingService(context);

                //Act && Assert
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await loggingService.LogErrorAsync(null));
            }
        }
    }
}
