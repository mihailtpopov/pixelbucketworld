﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.MessageServiceTests
{
    [TestClass]
    public class AcceptJurorInvitation_Should
    {
        [TestMethod]
        public async Task AcceptJurorInvitation_ShouldThrowArgumentNullException_IfInvalidInvitationMessagesIsPassed()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                //Arrange
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await unitOfWork.MessageService.AcceptJurorInvitationAsync(5000, 1));
            }   
        }

        [TestMethod]
        public async Task AcceptJurorInvitation_ShouldThrowInvalidOperationException_IfInvitationalMessageReceiverIdIsDifferentThanUserId()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                //Arrange
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                await Assert.ThrowsExceptionAsync<InvalidOperationException>(async () => await unitOfWork.MessageService.AcceptJurorInvitationAsync(1, 1));
            }
        }
    }
}
