﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Consts.Strings;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.PhotoAssesmentServiceTests
{
    [TestClass]
    public class CreateAsync_Should
    {
        [TestMethod]
        public async Task CreateAsync_Should_Throw_IfPhotoNotFound()
        {
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);
                var newAssesment = new PhotoAssessment()
                {
                    Score = 5,
                    Comment = "Best newAssesment score",
                    UserId = 1,
                    PhotoId = 9999,
                };

                var unitOfWork = new UnitOfWork(context);

                // Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(
                    () => unitOfWork.PhotoAssessmentService.CreateAsync(newAssesment));
            }
        }

        [TestMethod]
        public async Task CreateAsync_Should_Throw_IfInvalidCOntestPhase()
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var newAssesment = new PhotoAssessment()
                {
                    Score = 5,
                    Comment = "Best newAssesment score",
                    UserId = 2,
                    PhotoId = 101,
                };

                var unitOfWork = new UnitOfWork(context);

                // Act & Assert
                await Assert.ThrowsExceptionAsync<InvalidOperationException>(
                    () => unitOfWork.PhotoAssessmentService.CreateAsync(newAssesment));
            }
        }

        [TestMethod]
        public async Task CreateAsync_Should_Throw_IfInvalideRater()
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var newAssesment = new PhotoAssessment()
                {
                    Score = 5,
                    Comment = "Best newAssesment score",
                    UserId = 2, // a junkie
                    PhotoId = 5,
                };

                var unitOfWork = new UnitOfWork(context);

                // Act & Assert
                await Assert.ThrowsExceptionAsync<UnauthorizedAccessException>(
                    () => unitOfWork.PhotoAssessmentService.CreateAsync(newAssesment));
            }
        }

        [TestMethod]
        public async Task CreateAsync_Should_Throw_IfAlreadyRated()
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var newAssesment = new PhotoAssessment()
                {
                    Score = 10,
                    Comment = "Best photo!",
                    UserId = 1,
                    PhotoId = 5,
                };

                var unitOfWork = new UnitOfWork(context);

                // Act & Assert
                await Assert.ThrowsExceptionAsync<InvalidOperationException>(
                    () => unitOfWork.PhotoAssessmentService.CreateAsync(newAssesment));
            }
        }

        [TestMethod]
        [DataRow(-1)]
        [DataRow(11)]
        public async Task CreateAsync_Should_Throw_IfScoreNotInRange(int score)
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var newAssesment = new PhotoAssessment()
                {
                    Score = score,
                    Comment = "newAssesment score",
                    UserId = 1,
                    PhotoId = 5,
                };

                var unitOfWork = new UnitOfWork(context);

                // Act & Assert
                await Assert.ThrowsExceptionAsync<InvalidOperationException>(
                    () => unitOfWork.PhotoAssessmentService.CreateAsync(newAssesment));
            }
        }

        [TestMethod]
        [DataRow("")]
        [DataRow(null)]
        public async Task CreateAsync_Should_Throw_IfCommentMissing(string comment)
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var newAssesment = new PhotoAssessment()
                {
                    Score = 0,
                    Comment = comment,
                    UserId = 1,
                    PhotoId = 5,
                };

                var unitOfWork = new UnitOfWork(context);

                // Act & Assert
                await Assert.ThrowsExceptionAsync<InvalidOperationException>(
                    () => unitOfWork.PhotoAssessmentService.CreateAsync(newAssesment));
            }
        }

        [TestMethod]
        public async Task CreateAsync_Should_Assign_DefaultComment_IfZeroScore()
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                #region Seed new contest with one photo
                var newContest = new Contest()
                {
                    Id = 999,
                    CategoryId = 2,
                    Name = "Motorcicle touring",
                    PhaseId = 4,
                    TypeId = 1,
                    PhaseOneStartingDate = DateTime.Now.AddDays(-4),
                    PhaseOneFinishingDate = DateTime.Now.AddDays(-3),
                    PhaseTwoStartingDate = DateTime.Now.AddDays(-2),
                    PhaseTwoFinishingDate = DateTime.Now.AddDays(-1),
                    PhotoUrl = "/images/ContestPhotos/Nature.jpg"
                };
                context.Contests.Add(newContest);

                var newPhoto = new Photo()
                {
                    Id = 999,
                    Title = "Photo999",
                    Story = "Photo 999 story.",
                    PhotoUrl = "/images/UploadedPhotos/pic999.jpg",
                    UserId = 6,
                    ContestId = 999,
                    ContestRanking = null,
                };
                context.Photos.Add(newPhoto);

                var newCPL = new ContestParticipantLink()
                {
                    UserId = 6,
                    ContestId = 999,
                    IsAccepted = null,
                    ParticipationDate = null,
                    UploadingPhotoTime = DateTime.Now.AddDays(-3.5)
                };
                context.ContestParticipantLinks.Add(newCPL);

                await context.SaveChangesAsync();

                #endregion

                var unitOfWork = new UnitOfWork(context);

                // Act
                var newAssesment = new PhotoAssessment()
                {
                    Score = 0,
                    Comment = "newAssesment score",
                    UserId = 1,
                    PhotoId = 999,
                };

                await unitOfWork.PhotoAssessmentService.CreateAsync(newAssesment);

                // Assert
                var assesmentComment = context.Entry(newAssesment).Entity.Comment;

                Assert.AreEqual(
                    AssesmentConstants.DEFAULT_COMMENT_FOR_ZERO_SCORE,
                    assesmentComment);
            }
        }

    }
}
