﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.PhotoAssesmentServiceTests
{
    [TestClass]
    public class GetByIdAsync_Should
    {
        [TestMethod]
        public async Task GetByIdAsync_Should_Return_Correctly()
        {
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                var photoAssesment = await unitOfWork
                    .PhotoAssessmentService.GetByIdAsync(1);

                // Act & Assert
                Assert.AreEqual(1, photoAssesment.UserId);
                Assert.AreEqual(5, photoAssesment.PhotoId);
            }
        }




    }
}
