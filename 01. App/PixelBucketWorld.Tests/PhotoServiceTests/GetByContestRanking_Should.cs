﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.PhotoServiceTests
{
    [TestClass]
    public class GetByContestRanking_Should
    {
        [TestMethod]
        public async Task GetByContestRanking_Should_ReturnCorrectEntity()
        {
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                // Act
                var contests = await unitOfWork
                    .PhotoService.GetByContestRanking(101, 1);

                // Assert
                Assert.AreEqual(101, contests.First().Id);
            }

        }

    }
}
