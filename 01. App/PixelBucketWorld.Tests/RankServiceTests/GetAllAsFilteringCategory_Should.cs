﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Contracts;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.RankServiceTests
{
    [TestClass]
    public class GetAllAsFilteringCategory_Should
    {
        [TestMethod]
        public async Task GetAllAsFilteringCategory_ShouldReturnCorrectType()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                //Arrange
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                //Act
                var ranksAsFilteringCategory = await unitOfWork.RankService.GetAllAsFilteringCategoryAsync();

                //Assert
                Assert.IsInstanceOfType(ranksAsFilteringCategory, typeof(IEnumerable<IUserFilteringCategory>));
            }
        }

        [TestMethod]
        public async Task GetAllAsFilteringCategory_ShouldReturnCorrectCount()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                //Arrange
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                //Act
                var ranksAsFilteringCategory = (await unitOfWork.RankService.GetAllAsFilteringCategoryAsync()).ToList();
                //Assert
                Assert.AreEqual(4, ranksAsFilteringCategory.Count());
            }
        }
    }
}
