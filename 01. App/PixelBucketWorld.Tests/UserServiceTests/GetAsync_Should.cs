﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using PixeBucketWorld.Data.Models.FilteringModels;
using PixelBucketWorld.Services;
using PixelBucketWorld.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.UserServiceTests
{
    [TestClass]
    public class GetAsync_Should
    {
        [TestMethod]
        public async Task GeatAsync_ShouldReturnCorrectUsers_WhenFilterByFirstName()
        {

            // Arrange

            var filter1 = new UserFilteringCriterias() { FirstName = "Ben" };

            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                context.Database.EnsureDeleted();
                context.Users.AddRange(Util.SeedUsers());
                context.SaveChanges();

                var unitOfWork = new UnitOfWork(context);

                //Act
                var users = await unitOfWork.UserService.GetAsync(filter1);

                //Assert
                Assert.AreEqual(1, users.Count());
            }
        }

        [TestMethod]
        public async Task GeatAsync_ShouldReturnCorrectUsers_WhenFilterByLastName()
        {

            // Arrange

            var filter1 = new UserFilteringCriterias() { LastName = "Gunn" };

            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                context.Database.EnsureDeleted();
                context.Users.AddRange(Util.SeedUsers());
                context.SaveChanges();

                var unitOfWork = new UnitOfWork(context);

                //Act
                var users = await unitOfWork.UserService.GetAsync(filter1);

                //Assert
                Assert.AreEqual(1, users.Count());
            }
        }

        [TestMethod]
        public async Task GeatAsync_ShouldReturnCorrectUsers_WhenFilterByRanks()
        {

            // Arrange

            var filter1 = new UserFilteringCriterias() { RankSelectOptions = new List<int>() { 1 } };

            var filter2 = new UserFilteringCriterias() { RankSelectOptions = new List<int>() { 1, 2 } };

            var filter3 = new UserFilteringCriterias() { RankSelectOptions = new List<int>() { 1, 2, 3 } };

            var filter4 = new UserFilteringCriterias() { RankSelectOptions = new List<int>() { 1, 2, 3, 4 } };

            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                context.Database.EnsureDeleted();
                context.Users.AddRange(Util.SeedUsers());
                context.SaveChanges();

                var unitOfWork = new UnitOfWork(context);

                //Act
                var users1 = await unitOfWork.UserService.GetAsync(filter1);
                var users2 = await unitOfWork.UserService.GetAsync(filter2);
                var users3 = await unitOfWork.UserService.GetAsync(filter3);
                var users4 = await unitOfWork.UserService.GetAsync(filter4);

                //Assert
                Assert.AreEqual(2, users1.Count());
                Assert.AreEqual(3, users2.Count());
                Assert.AreEqual(4, users3.Count());
                Assert.AreEqual(5, users4.Count());
            }
        }

        [TestMethod]
        public async Task GetAsync_ShoudlReturnCorrectUsersCount_WhenFilterByRoles()
        {
            var filter1 = new UserFilteringCriterias() { RoleSelectOptions = new List<int>() { 1 } };
            var filter2 = new UserFilteringCriterias() { RoleSelectOptions = new List<int>() { 2 } };

            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                //Act
                var users = await unitOfWork.UserService.GetAsync(filter1);
                var users2 = await unitOfWork.UserService.GetAsync(filter2);

                //Assert
                Assert.AreEqual(1, users.Count());
                Assert.AreEqual(5, users2.Count());
            }
        }
    }
}
