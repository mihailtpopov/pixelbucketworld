﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using PixeBucketWorld.Data.Models;
using System;

namespace PixelBucketWorld.API.Helpers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class Authenticate : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var user = (User)context.HttpContext.Items["User"];
            // Currently an object. Cast it to User if such is needed.

            if (user == null)
            {
                context.Result = new JsonResult(new { message = "Unauthenticated" }) { StatusCode = StatusCodes.Status401Unauthorized };
                return;
            }
        }
    }
}
