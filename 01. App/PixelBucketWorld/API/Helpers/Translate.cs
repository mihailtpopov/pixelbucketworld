﻿using System;
using System.Text;

namespace PixelBucketWorld.API.Helpers
{
    public static class Translate
    {
        public static (string userName, string password) Header(string token)
        {
            string authenticationToken = token.Split(' ')[1];
            var decodedAuthenticationToken = Encoding.UTF8.GetString(Convert.FromBase64String(authenticationToken));
            var usernamePasswordArray = decodedAuthenticationToken.Split(':');
            var userName = usernamePasswordArray[0];
            var password = usernamePasswordArray[1];

            return (userName, password);
        }

    }
}
