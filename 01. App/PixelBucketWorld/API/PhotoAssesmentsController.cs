﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PixeBucketWorld.Data.Models;
using PixeBucketWorld.Data.Models.Logging.Enums;
using PixelBucketWorld.API.Helpers;
using PixelBucketWorld.Consts.Strings;
using PixelBucketWorld.Helpers.Mappers.InputModels.PhotoAssesmentModels;
using PixelBucketWorld.Helpers.Mappers.OutputModels;
using PixelBucketWorld.Map;
using PixelBucketWorld.Services;
using PixelBucketWorld.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace PixelBucketWorld.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhotoAssesmentsController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ILoggingService loggingService;

        public PhotoAssesmentsController(IUnitOfWork unitOfWork, ILoggingService loggingService)
        {
            this.unitOfWork = unitOfWork;
            this.loggingService = loggingService;
        }

        /// <summary>
        /// Organizers can get assemnet by id.
        /// Token authentication.
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200">Returns the assesment.</response>
        /// <response code="404">If assesment not found.</response>
        [Authorize(UserRoleConsts.ORGANISER)]
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetById(int id)
        {
            var photoAssesment = await this.unitOfWork.PhotoAssessmentService.GetByIdAsync(id);

            if (photoAssesment == null)
            {
                return NotFound();
            }

            return Ok(new OutputPhotoAssesmentFull(photoAssesment));
        }

        /// <summary>
        /// Jury can score photos.
        /// Token authentication.
        /// </summary>
        /// <remarks>Jurers can be every organiser and the invited participants that have accepted the invitation on time.</remarks>
        /// <param name="inputAssesment"></param>
        /// <response code="201">Returns the created assesment and its url.</response>
        /// <response code="400">
        /// If the contest is not in phase II.
        /// If the juror have already assessed the photo.
        /// If the score is outside the proper range 0-10.
        /// If comment is missing.
        /// </response>
        /// <response code="401">
        /// If invalid token.
        /// If the active user is participant and is not included in the jury.
        /// </response>
        /// <response code="404">If photo not found.</response>
        [Authenticate]
        [HttpPost("")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Create([FromBody] InputPhotoAssesment inputAssesment)
        {
            // get the active user
            var user = (User)HttpContext.Items["User"];

            var newAssesment = InputMapper.Map(inputAssesment);

            newAssesment.UserId = user.Id;

            try
            {
                var createdAssesment = await this.unitOfWork.PhotoAssessmentService.CreateAsync(newAssesment);
                await this.unitOfWork.SaveAsync();

                await this.loggingService.LogInformationAsync(InformationLogEvent.INSERT_ITEM, null, createdAssesment);
                await this.unitOfWork.SaveAsync();

                string url = UrlHelper.GetUrl(Request, createdAssesment.Id);

                return Created(url, new OutputPhotoAssesmentPartial(createdAssesment));

            }
            catch(ArgumentNullException e)
            {
                return NotFound(e.Message);
            }
            catch(UnauthorizedAccessException e)
            {
                return Unauthorized(e.Message);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }       
        }
    }
}
