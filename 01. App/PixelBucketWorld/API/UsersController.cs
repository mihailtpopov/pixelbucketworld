﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using PixeBucketWorld.Data.Models;
using PixeBucketWorld.Data.Models.FilteringModels;
using PixeBucketWorld.Data.Models.Logging.Enums;
using PixelBucketWorld.API.Helpers;
using PixelBucketWorld.Consts.Strings;
using PixelBucketWorld.Helpers.Mappers.OutputModels;
using PixelBucketWorld.Helpers.Paging;
using PixelBucketWorld.Map;
using PixelBucketWorld.Map.InputModels.InputUserModels;
using PixelBucketWorld.Map.OutputModels;
using PixelBucketWorld.Services;
using PixelBucketWorld.Services.Contracts;
using PixelBucketWorld.Services.Helpers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PixelBucketWorld.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly UserManager<User> userManager;
        private readonly ILoggingService loggingService;
        private readonly AppSettings appSettings;

        public UsersController(IUnitOfWork unitOfWork, UserManager<User> userManager, ILoggingService loggingService, IOptions<AppSettings> appSettings)
        {
            this.unitOfWork = unitOfWork;
            this.userManager = userManager;
            this.loggingService = loggingService;
            this.appSettings = appSettings.Value;
        }

        /// <summary>
        /// Registered users can authenticate.
        /// Authentication by username and password in request header.
        /// </summary>
        /// <remarks>
        /// Sample request: POST api/users/authenticate
        /// </remarks>
        /// <response code="200">Returns a JSON object containing a token for authentication/authorization in next requests and the date/time of token expiration.</response>
        /// <response code="400">If authentication failed.</response>
        [HttpPost("authenticate")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Authenticate()
        {
            var credentials = Translate.Header(Request.Headers["Authorization"]);

            var user = await this.userManager.FindByNameAsync(credentials.userName);
            var result = await this.userManager.CheckPasswordAsync(user, credentials.password);

            if (!result)
            {
                return BadRequest("Incorrect username or password");
            }

            var role = (await this.userManager.GetRolesAsync(user)).First();

            var token = JWToken.GenerateToken(user, this.appSettings, role);

            return Ok(token);
        }

        /// <summary>
        /// Anonymous users can register.
        /// </summary>
        /// <param name="registerUser"></param>
        /// <response code="201">Returns the newly created user and its url.</response>
        /// <response code="400">If username or passowrd are not valud.</response>
        [HttpPost("")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RegisterAsync(InputRegisterUser registerUser)
        {
            User user = InputMapper.Map(registerUser);
            var result = await userManager.CreateAsync(user, registerUser.Password);

            if (result.Succeeded)
            {
                this.unitOfWork.UserService.SetUserInitialDetails(user);
                await this.userManager.AddToRoleAsync(user, UserRoleConsts.PARTICIPANT);

                await this.loggingService.LogInformationAsync(InformationLogEvent.INSERT_ITEM, null, user);
                await this.unitOfWork.SaveAsync();

                var createdUser = await this.unitOfWork.UserService.GetByIdAsync(user.Id);
                await this.unitOfWork.UserService.LoadRankAsync(createdUser);

                string url = UrlHelper.GetUrl(Request, createdUser.Id);

                return Created(url,new OutputUserPartial(createdUser));
            }
            else
            {
                string errorResult = "";
                foreach (var item in result.Errors)
                {
                    errorResult += Environment.NewLine + item.Description;
                }
                return BadRequest(errorResult);
            }

        }

        /// <summary>
        /// Organizers can get specific user's profile
        /// Registered uzers can get theirs profile.
        /// Token authentication.
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200">Returns the searched user data.</response>
        /// <response code="404">If user with the specified id is not found.</response>
        /// <response code="401">If the active user is participant and tries to </response>
        [Authorize(UserRoleConsts.ORGANISER)]
        [HttpGet("profile/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Profile(int id)
        {
            var activeUser = (User)HttpContext.Items["User"];
            var activeUserRole = HttpContext.Items["Role"].ToString();

            if (activeUserRole != UserRoleConsts.ORGANISER && activeUser.Id != id)
            {
                return Unauthorized("Unauthorized");
            }

            var user = await this.unitOfWork.UserService.GetByIdAsync(id);

            if (user != null)
            {
                await this.unitOfWork.UserService.LoadRankAsync(user);
                return Ok(OutputMapper.Map(user));
            }
            else
            {
                return NotFound();
            }

        }

        /// <summary>
        /// Organizers can brawse participants by rank.
        /// Token authentication.
        /// </summary>
        /// <remarks>
        /// Sample request: GET api/users/participants?ranks=1,2
        /// Assign ranks as follows:\
        /// for junkie - rank = 1\
        /// for enthusiast - rank = 2\
        /// for master - rank = 3\
        /// for Wise and Benevolent Photo Dictator - rank = 4\
        /// Any combination of searched ranks is possible.Use ',' to separate.\
        /// If no ranks are 
        /// </remarks>
        /// <param name="ranks">comma separated ranks to include</param>
        /// <param name="pagingParameters"></param>
        /// <response code="200">Returns collection of participants filterd by ranks.</response>
        [Authorize(UserRoleConsts.ORGANISER)]
        [HttpGet("participants")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByRank([FromQuery] string ranks, [FromQuery] PagingParameters pagingParameters)
        {
            var ranksArray = (ranks.Split(',', StringSplitOptions.RemoveEmptyEntries))
                .Select(r => int.Parse(r));

            var filter = new UserFilteringCriterias();
            filter.RoleSelectOptions.Add(2);
            foreach (var rank in ranksArray)
            {
                filter.RankSelectOptions.Add(rank);
            }

            var users = (await this.unitOfWork.UserService.GetAsync(filter)).Select(u => new OutputUserPartial(u));

            var pagedUsers = PagedList<OutputUserPartial>.ToPagedList
                (
                users,
                pagingParameters.Pagenumber,
                pagingParameters.PageSize,
                Request
                );

            WriteHeaders.PagedResposeParameters(Response, pagedUsers);

            return Ok(pagedUsers);
        }

        /// <summary>
        /// Participants can get the contests they have enrolled.
        /// Oragnizers can get the enrolled contest for a specific user.
        /// Token authentication.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pagingParameters"></param>
        /// <response code="200">Returns collection enrolled contests for the user.</response>
        /// <response code="401">
        /// If invalid token..
        /// If active user is not organizer and if the id in the request does not correspond to the id of the active user.
        /// </response>
        [Authenticate]
        [HttpGet("{id}/contests")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetEnrolledContests(int id, [FromQuery] PagingParameters pagingParameters)
        {
            // get the active user
            var user = (User)HttpContext.Items["User"];

            if (user.Id != id)
            {
                return Unauthorized("Unauthorized");
            }

            // gets all contest the user eve enrolled
            var userContests = (await this.unitOfWork.ContestService.GetParticipantContestsAsync(id))
                .Select(c => new OutputContestPartial(c));

            var pagedUserContests = PagedList<OutputContestPartial>.ToPagedList
                (
                userContests,
                pagingParameters.Pagenumber,
                pagingParameters.PageSize,
                Request
                );

            WriteHeaders.PagedResposeParameters(Response, pagedUserContests);

            return Ok(pagedUserContests);

        }


    }
}
