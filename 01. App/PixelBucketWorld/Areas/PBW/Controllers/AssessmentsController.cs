﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PixelBucketWorld.Consts.Strings;
using PixelBucketWorld.Map;
using PixelBucketWorld.Services;
using PixelBucketWorld.ViewModels.PhotoVMs;
using System.Threading.Tasks;

namespace PixelBucketWorld.Areas.PBW.Controllers
{
    [Area("PBW")]
    [Authorize(Roles = UserRoleConsts.ORGANISER)]
    public class AssessmentsController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public AssessmentsController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([FromForm] PhotoToRateCardViewModel model)
        {
            
            var photoAssessment = InputMapper.Map(model);

            if(model.IsFittingCategory == true)
            {
                photoAssessment.Score = 0;
            }

            await this.unitOfWork.PhotoAssessmentService.CreateAsync(photoAssessment);
            await this.unitOfWork.SaveAsync();

            return View("SuccessfullyRatePhoto");

        }
    }
}
