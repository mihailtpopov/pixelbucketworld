﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PixelBucketWorld.Consts.Numbers;
using PixelBucketWorld.Models;
using PixelBucketWorld.Services;
using PixelBucketWorld.ViewModels.ContestVMs;
using PixelBucketWorld.ViewModels.IndexVMs;
using PixelBucketWorld.ViewModels.PhotoVMs;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace PixelBucketWorld.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IUnitOfWork unitOfWork;

        public HomeController(ILogger<HomeController> logger, IUnitOfWork unitOfWork)
        {
            _logger = logger;
            this.unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int? page)
        {
            var latestWinningPhotos = await this.unitOfWork.PhotoService.GetLatestWinningPhotoAsync(3);
            var openPhaseOneContests = await this.unitOfWork.ContestService.GetContestsByPhaseAndTypeAsync(ContestTypeConsts.OPEN, PhaseConsts.PHASE_ONE);

            var indexVM = new IndexViewModel();

            indexVM.LatestWinningPhotos = latestWinningPhotos.Select(lwp => new PhotoCardViewModel(lwp));
            indexVM.OpenPhaseOneContests = openPhaseOneContests.Select(c => new ContestCardViewModel(c));

            return View(indexVM);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult PageNotFound()
        {
            return View();
        }
    }
}
