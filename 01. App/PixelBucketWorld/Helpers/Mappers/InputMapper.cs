﻿using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Helpers.Mappers.InputModels.InputCategoryModels;
using PixelBucketWorld.Helpers.Mappers.InputModels.InputInvitationalMessageModels;
using PixelBucketWorld.Helpers.Mappers.InputModels.PhotoAssesmentModels;
using PixelBucketWorld.Map.InputModels.InputContestModels;
using PixelBucketWorld.Map.InputModels.InputUserModels;
using PixelBucketWorld.ViewModels.ContestVMs;
using PixelBucketWorld.ViewModels.PhotoVMs;
using PixelBucketWorld.ViewModels.UserVMs;

namespace PixelBucketWorld.Map
{
    public static class InputMapper
    {
        // Contest mappers
        public static Contest Map(CreateContestViewModel inputContest)
        {
            return new Contest()
            {
                TypeId = inputContest.Type,
                CategoryId = inputContest.Category,
                Name = inputContest.Title,
                PhaseOneStartingDate = inputContest.PhaseOneStartingDate,
                PhaseOneFinishingDate = inputContest.PhaseOneFinishingDate,
                PhaseTwoStartingDate = inputContest.PhaseTwoStartingDate,
                PhaseTwoFinishingDate = inputContest.PhaseTwoFinishingDate,
                PhaseId = 1,
            };
        }

        public static Contest Map(InputContestModel inputContest)
        {
            return new Contest()
            {
                TypeId = inputContest.TypeId,
                CategoryId = inputContest.CategoryId,
                Name = inputContest.Title,
                PhaseOneStartingDate = inputContest.PhaseOneStartingDate,
                PhaseOneFinishingDate = inputContest.PhaseOneFinishingDate,
                PhaseTwoStartingDate = inputContest.PhaseTwoStartingDate,
                PhaseTwoFinishingDate = inputContest.PhaseTwoFinishingDate,
                PhaseId = 1,
            };
        }

        public static User Map(EditUserViewModel editedUser, User existingUser)
        {
            existingUser.FirstName = editedUser.FirstName;
            existingUser.LastName = editedUser.LastName;
            existingUser.Email = editedUser.Email;

            return existingUser;
        }

        // User mappers
        public static User Map(UserRegisterViewModel registerViewModel)
        {
            return new User()
            {
                FirstName = registerViewModel.FirstName,
                LastName = registerViewModel.LastName,
                UserName = registerViewModel.Username,
            };
        }

        public static User Map(InputRegisterUser registerViewModel)
        {
            return new User()
            {
                FirstName = registerViewModel.FirstName,
                LastName = registerViewModel.LastName,
                UserName = registerViewModel.Username,
                Email = registerViewModel.Email
            };
        }

        //public static User Map(InputRegisterUser user)
        //{
        //    return new User()
        //    {
        //        FirstName = user.FirstName,
        //        LastName = user.LastName,
        //        UserName = user.Username,
        //        RankId = 1
        //    };
        //}


        public static Category Map(InputCategory category)
        {
            return new Category() { Name = category.Name };
        }

        public static Photo Map(UploadPhotoViewModel photo)
        {
            return new Photo()
            {
                ContestId = photo.ContestId,
                Story = photo.PhotoStory,
                Title = photo.PhotoTitle,
                UserId = photo.UserId
            };
        }

        public static PhotoAssessment Map(PhotoToRateCardViewModel photoCardViewModel)
        {
            return new PhotoAssessment()
            {
                UserId = photoCardViewModel.RaterId,
                PhotoId = photoCardViewModel.PhotoId,
                Score = photoCardViewModel.Score,
                Comment = photoCardViewModel.Comment
            };
        }

        public static PhotoAssessment Map(InputPhotoAssesment inputAssesment)
        {
            return new PhotoAssessment()
            {
                PhotoId = inputAssesment.PhotoId,
                Score = inputAssesment.Score,
                Comment = inputAssesment.Comment
            };
        }

        public static InvitationalMessage Map(InputInivitationalMessage message, User sender)
        {
            return new InvitationalMessage()
            {
                Title = "",
                Content = "",
                ReceiverId = message.ReceiverId,
                SenderId = sender.Id,
                ContestId = message.ContestId,
            };

        }
    }
}
