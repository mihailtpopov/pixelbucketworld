﻿namespace PixelBucketWorld.Helpers.Mappers.InputModels.InputContestParticipantsContesterModels
{
    public class InputAcceptedContestInvitation
    {
        public int ContestId { get; set; }

        public int UserId { get; set; }
    }
}
