﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace PixelBucketWorld.Helpers.Mappers.InputModels.InputPhoto
{
    public class InputPhoto
    {
        [Required, MinLength(3, ErrorMessage = "Photo story must be at least {1} characters")]
        public string Title { get; set; }

        [Required, MinLength(8, ErrorMessage = "Photo title must be at least {1} characters")]
        public string Story { get; set; }

        [Required]
        public int ContestId { get; set; }

        public IFormFile File { get; set; }



    }
}
