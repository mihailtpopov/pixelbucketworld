﻿namespace PixelBucketWorld.Helpers.Mappers.InputModels.PhotoAssesmentModels
{
    public class InputPhotoAssesment
    {
        public int Score { get; set; }

        public string Comment { get; set; }

        public int PhotoId { get; set; }

    }
}
