﻿namespace PixelBucketWorld.Helpers.Mappers.OutputModels
{
    public class OutputCategoryPartial
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
