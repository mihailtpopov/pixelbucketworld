﻿using PixeBucketWorld.Data.Models;

namespace PixelBucketWorld.Helpers.Mappers.OutputModels
{
    public class OutputPhotoAssesmentFull: OutputPhotoAssesmentPartial
    {
        public OutputPhotoAssesmentFull(PhotoAssessment asstesment)
            : base(asstesment)
        {
            this.Rater = new OutputUserPartial(asstesment.User);
            this.Photo = new OutputPhotoPartial(asstesment.Photo);
        }

        public OutputUserPartial Rater { get; set; }
        public OutputPhotoPartial Photo { get; set; }
    }
}
