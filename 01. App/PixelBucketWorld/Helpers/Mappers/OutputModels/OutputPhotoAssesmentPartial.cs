﻿using PixeBucketWorld.Data.Models;

namespace PixelBucketWorld.Helpers.Mappers.OutputModels
{
    public class OutputPhotoAssesmentPartial
    {
        public OutputPhotoAssesmentPartial(PhotoAssessment asstesment)
        {
            this.Id = asstesment.Id;
            this.Score = asstesment.Score;
            this.Comment = asstesment.Comment;
        }

        public int Id { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
    }
}
