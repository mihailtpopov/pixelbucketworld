﻿using PixeBucketWorld.Data.Models;

namespace PixelBucketWorld.Helpers.Mappers.OutputModels
{
    public class OutputUserBase
    {
        public OutputUserBase()
        {

        }

        public OutputUserBase(User user)
        {
            Id = user.Id;
            FirstName = user.FirstName;
            LastName = user.LastName;

        }

        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

    }
}
