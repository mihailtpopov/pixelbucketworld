﻿using PixeBucketWorld.Data.Models;

namespace PixelBucketWorld.Helpers.Mappers.OutputModels
{
    public class OutputUserPartial : OutputUserBase
    {
        public OutputUserPartial()
        {

        }

        public OutputUserPartial(User user)
            :base(user)
        {
            if (user.Rank != null)
            {
                Rank = user.Rank.Name;
            }
            else
            {
                Rank = "";
            }
        }

        public string Rank { get; set; }
    }
}
