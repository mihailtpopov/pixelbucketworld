﻿using Microsoft.AspNetCore.Http;
using PixelBucketWorld.API.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PixelBucketWorld.Helpers.Paging
{
    public class PagedList<T> : List<T>
    {
        public PagedList(List<T> items, int count, int pageNumber, int pageSize)
        {
            this.TotalCount = count;
            this.PageSize = pageSize;
            this.CurrentPage = pageNumber;
            this.TotalPages = (int)Math.Ceiling(count / (double)pageSize);

            AddRange(items);
        }

        public PagedList(List<T> items, int count, int pageNumber, int pageSize, HttpRequest request)
            : this(items, count, pageNumber, pageSize)
        {

            //string query = GetQueryString(request);

            if (this.HasNext)
            {
                string query = GetQueryString(request, pageNumber + 1, pageSize);
                this.UrlNext = UrlHelper.GetUrlQuery(request, query);
            }

            if (this.HasPrevious)
            {
                string query = GetQueryString(request, pageNumber - 1, pageSize);
                this.UrlPrevious = UrlHelper.GetUrlQuery(request, query);
            }

        }

        public int CurrentPage { get; private set; }
        public int TotalPages { get; private set; }
        public int PageSize { get; private set; }
        public int TotalCount { get; private set; }

        public bool HasPrevious => CurrentPage > 1;
        public bool HasNext => CurrentPage < TotalPages;

        public string UrlNext { get; set; }
        public string UrlPrevious { get; set; }

        public static PagedList<T> ToPagedList(IEnumerable<T> sourse, int pageNumber, int pageSize)
        {
            var count = sourse.Count();
            var items = sourse.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

            return new PagedList<T>(items, count, pageNumber, pageSize);
        }

        public static PagedList<T> ToPagedList(IEnumerable<T> sourse, int pageNumber, int pageSize, HttpRequest request)
        {
            var count = sourse.Count();
            var items = sourse.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

            return new PagedList<T>(items, count, pageNumber, pageSize, request);
        }

        private string GetQueryString(HttpRequest request, int pageNumber, int pageSize)
        {
            List<string> queryParts = new List<string>();

            var requestQuery = request.Query;

            foreach (var item in requestQuery)
            {
                if (item.Key.ToLower() != "pagesize" && item.Key.ToLower() != "pagenumber")
                {
                    queryParts.Add(item.Key + "=" + item.Value);
                }
            }

            queryParts.Add("pagenumber=" + pageNumber);
            queryParts.Add("pagesize=" + pageSize);

            return string.Join('&', queryParts);
        }

        private string GetQueryString(HttpRequest request)
        {
            var requestQuery = request.Query;
            if (requestQuery.Count == 0)
            {
                return "";
            }

            List<string> queryParts = new List<string>();

            foreach (var item in requestQuery)
            {
                if (item.Key.ToLower() != "pagesize" && item.Key.ToLower() != "pagenumber")
                {
                    queryParts.Add(item.Key + "=" + item.Value);
                }
            }

            return string.Join('&', queryParts);
        }

    }
}
