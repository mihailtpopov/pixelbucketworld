﻿namespace PixelBucketWorld.Helpers.Paging
{
    public class PagingParameters
    {
        const int maxPageSize = 10;
        private int pageSize = 3;

        public int Pagenumber { get; set; } = 1;

        public int PageSize
        {
            get
            {
                return this.pageSize;
            }
            set
            {
                this.pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }
    }
}
