﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace PixelBucketWorld.Helpers
{
    public static class Uploader
    {
        // filePath = full path + file name
        public static async Task Upload(IFormFile file, string filePath)
        {
            var fileName = file.FileName;

            //var fullPath = Path.Combine(pathToSave, fileName);

            var ext = fileName.Substring(fileName.LastIndexOf('.')).ToLower();

            List<string> allowedFileExt = new List<string>() { ".jpg", ".png" };

            if (!allowedFileExt.Contains(ext))
            {
                throw new InvalidOperationException("Please Upload image of type .jpg, .png.");

            }

            int MaxContentLength = 1024 * 1024 * 2; //Size = 2 MB

            if (file.Length > MaxContentLength)
            {
                throw new InvalidOperationException("Please upload file up to 2 MB.");
            }

            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
        }

    }
}
