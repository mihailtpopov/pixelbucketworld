﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.DbContextExtensions;
using PixelBucketWorld.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace PixelBucketWorld.Middlewares
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate next;

        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext httpContext, ILoggingService loggingService, PBWDbContext dbContext)
        {
            try
            {
                await next.Invoke(httpContext);
            }
            catch (Exception e)
            {
                dbContext.DetachAllEntities();

                await loggingService.LogErrorAsync(e);

                dbContext.SaveChanges();

                var path = httpContext.Request.GetDisplayUrl();

                if (!path.Contains("api"))
                {
                    httpContext.Response.Redirect("/Home/PageNotFound");
                }

                httpContext.Response.StatusCode = 400;
                await httpContext.Response.WriteAsync(e.Message);
            }
        }

    }
}
