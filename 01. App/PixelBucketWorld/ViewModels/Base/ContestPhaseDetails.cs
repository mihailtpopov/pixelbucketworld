﻿using PixeBucketWorld.Data.Models;

namespace PixelBucketWorld.ViewModels.Base
{
    public class ContestPhaseDetails
    {
        public ContestPhaseDetails()
        {

        }

        public ContestPhaseDetails(Contest contest)
        {
            this.PhotoUrl = contest.PhotoUrl;
            this.Title = contest.Name;
            this.Category = contest.Category.Name;
            this.Type = contest.Type.Name;
        }

        public string PhotoUrl { get; set; }

        public string Title { get; set; }

        public string Category { get; set; }

        public string Type { get; set; }
    }
}
