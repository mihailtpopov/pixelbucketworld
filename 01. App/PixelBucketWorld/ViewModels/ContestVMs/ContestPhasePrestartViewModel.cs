﻿using PixeBucketWorld.Data.Models;
using PixelBucketWorld.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PixelBucketWorld.ViewModels.ContestVMs
{
    public class ContestPhasePrestartViewModel : ContestPhaseDetails
    {
        public ContestPhasePrestartViewModel(Contest contest) 
            : base(contest)
        {
            this.Title = contest.Name;
            this.Category = contest.Category.Name;
            this.Type = contest.Type.Name;
            this.CurrentPhaseDiffMillisec = (contest.PhaseOneStartingDate - DateTime.Now).TotalMilliseconds;
            this.CurrentNextPhaseDiffInMillsec = (contest.PhaseTwoStartingDate - DateTime.Now).TotalMilliseconds;
            this.InviteesLinks = contest.ParticipantsLink.Where(pl => pl.IsAccepted != null).ToList();
            this.JurorLinks = contest.JurorsLink;
            this.ContestId = contest.Id;
        }

        public int ContestId { get; set; }

        public double CurrentPhaseDiffMillisec { get; set; }

        public double CurrentNextPhaseDiffInMillsec { get; set; }

        public List<ContestParticipantLink> InviteesLinks { get; set; }

        public List<ContestJurorLink> JurorLinks { get; set; }
    }
}
