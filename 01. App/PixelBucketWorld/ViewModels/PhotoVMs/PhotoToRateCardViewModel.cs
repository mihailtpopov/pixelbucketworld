﻿using PixeBucketWorld.Data.Models;
using PixelBucketWorld.ViewModels.Contracts;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PixelBucketWorld.ViewModels.PhotoVMs
{
    public class PhotoToRateCardViewModel : IPhotoCardViewModel
    {
        public PhotoToRateCardViewModel()
        {

        }

        public PhotoToRateCardViewModel(Photo photo)
        {
            this.PhotoId = photo.Id;
            this.Story = photo.Story;
            this.Title = photo.Title;
            this.PhotoUrl = photo.PhotoUrl;
            this.Assessments = photo.PhotoAssessments;
        }

        public int PhotoId { get; set; }

        public int RaterId { get; set; }

        public string Comment { get; set; }

        public int Score { get; set; }

        public string Story { get; set; }

        public string Title { get; set; }

        public string PhotoUrl { get; set; }

        [Display(Name = "Irrelevant photo for current category")]
        public bool IsFittingCategory { get; set; }

        public List<PhotoAssessment> Assessments { get; set; }
    }
}
