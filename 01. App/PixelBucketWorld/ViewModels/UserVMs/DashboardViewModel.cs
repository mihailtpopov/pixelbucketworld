﻿using PixelBucketWorld.ViewModels.ContestVMs;
using System.Collections.Generic;

namespace PixelBucketWorld.ViewModels.UserVMs
{
    public class DashboardViewModel
    {
        public int RankingsPoints { get; set; }

        public string CurrentRank { get; set; }

        public int? PointsTillNextRank { get; set; }

        public List<ContestCardViewModel> ActiveContestsAsJuror { get; set; } = new List<ContestCardViewModel>();

        public List<ContestCardViewModel> OnGoingContestsAsJuror { get; set; } = new List<ContestCardViewModel>();

        public List<ContestCardViewModel> FinishedContestsAsJuror { get; set; } = new List<ContestCardViewModel>();


        public List<ContestCardViewModel> ActiveContests { get; set; } = new List<ContestCardViewModel>();

        public List<ContestCardViewModel> OnGoingContests { get; set; } = new List<ContestCardViewModel>();

        public List<ContestCardViewModel> FinishedContests { get; set; } = new List<ContestCardViewModel>();
    }
}
