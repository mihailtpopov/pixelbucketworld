﻿using PixeBucketWorld.Data.Contracts;
using System.Collections.Generic;

namespace PixelBucketWorld.ViewModels.UserVMs
{
    public class UserFiltrationViewModel
    {
        public Dictionary<string, List<IUserFilteringCategory>> Categories { get; set; } = new Dictionary<string, List<IUserFilteringCategory>>();
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
