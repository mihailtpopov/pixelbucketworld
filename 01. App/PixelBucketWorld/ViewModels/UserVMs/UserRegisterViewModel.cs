﻿using System.ComponentModel.DataAnnotations;

namespace PixelBucketWorld.ViewModels.UserVMs
{
    public class UserRegisterViewModel
    {
        [Display(Name = "First name")]
        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "First name must be between {1} and {0} characters")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "Last name must be between {1} and {0} characters")]
        public string LastName { get; set; }

        [Required]
        public string Username { get; set; }

        [Required, DataType(DataType.Password)]
        public string Password { get; set; }

        [Required, DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "Passwords do not match")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Subscribe me for incoming news")]
        public bool IsSubscribed { get; set; }
    }
}
