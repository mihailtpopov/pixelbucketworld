﻿$(document).ready((d) => {
    $("#photoSubmit").click(() => {
        var userChoice = window.confirm("Photo details can not be changed after submission. Would you like to continue?");

        if (userChoice) {
            $("#photoSubmissionForm").hide();
            var successfullUploadSpan = $("successfullUploadSpan");
            successfullUploadSpan.attr("class", "alert alert success");
            successfullUploadSpan.html("<strong>Congratulations!<strong> The photo is successfully uploaded. Good luck!");
            successfullUploadSpan.show();
        }
    })
}
)





